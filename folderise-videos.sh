#!/bin/bash

# Take directory full of MP4 videos named in the form SERIES_-_EPISODE and
# create directories for the series and move the episodes there
# Then look for, and delete, empty directories.

shopt -s nullglob
set -o pipefail
set -o errexit
set -o nounset

video_dir=${1:-/server/video}

moved=""
removed=""

# Look for MP4 files in the top level directory
# If they are names in the right format, create a series
# subdirectory and move the video there, replacing underscores
# in the series and episode names with spaces.

for video in $video_dir/*.mp4
do
	if [[ -f $video ]]
	then
		series="${video%%_-_*}"
		episode="${video#*_-_}"

		if [[ "${episode:0:1}" != '_' ]] && [[ ! -f "$series" ]]
		then
			series="${series//_/ }"
			episode="${episode//_ }"

			mkdir -m 777 -p "$series"
			mv "$video" "$series/$episode"
			
			moved="$moved\n* $(basename "$video")"
		fi
	fi
done

# Now look for empty directories and remove them

for dir in $video_dir/*
do
	if [[ -d "$dir" && -z $(find "$dir" -type f) ]]
	then
		rm -rf "$dir"
		removed="$removed\n* $(basename "$dir")"
	fi
done

# Report only if we've done something

if [[ -n "$moved" ]]
then
	echo -e "Moved:\n$moved"
	echo ""
fi

if [[ -n "$removed" ]]
then
	echo -e "Removed:\n$removed"
fi
